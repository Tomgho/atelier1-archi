Membre du projet: Albane CASTILLON, Tom GHOVANLOU, Lucas HOCDE, Louis MORAND

# Pour le web Statique:
-   Les pages de création de cartes et d'affichage des cartes sont fonctionnels en communiquant avec l'API

# Pour le web dynamique:

* le formulaire est créé dans le fichier carteForm.html. Pour y acceder, il faut faire __localhost:8082/addCarte__ dans un navigateur. Quand le formulaire est rempli, la carte s'affiche en cliquant sur +OK. Le bouton Cancel ne fonctionne pas. Pour le formulaire, il faut renseigner le nom, la description, une url d'une image, la family, l'affinity, le nombre d'hp, d'énergie, d'attaque et de défence.

* La Liste des cartes créé sont visibles : __localhost:8082/list__ . Seul le nom et la description s'affiche.

L'expliquation de pourquoi les prototypes respectent le pattern MVC ainsi que les avantages et inconvénients du web Statique et dynamique se trouvent dans le fichier TP1.md
