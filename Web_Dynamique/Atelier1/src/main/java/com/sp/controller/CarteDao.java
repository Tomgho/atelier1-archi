package com.sp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.sp.model.Carte;

@Service
public class CarteDao {
	private static List<Carte> myCarteList;
	private static Random randomGenerator;

	public CarteDao() {
		myCarteList=new ArrayList<>();
		randomGenerator = new Random();
		createCarteList();
	}

	public void createCarteList() {
		
		//TODO
		
	}
	public static List<Carte> getCarteList() {
		return myCarteList;
	}
	public Carte getCarteByName(String name){
		for (Carte carteBean : myCarteList) {
			if(carteBean.getName().equals(name)){
				return carteBean;
			}
		}
		return null;
	}
	public static Carte getRandomCarte(){
		int index=randomGenerator.nextInt(myCarteList.size());
		return myCarteList.get(index);
	}

	public Carte addCarte(String name, String description,String imgUrl,String family, String affinity,int hp,int energy, int attack, int defence) {
		Carte p=new Carte(name,description,imgUrl,family,affinity,hp,energy,attack,defence);
		myCarteList.add(p);
		return p;
	}


}
