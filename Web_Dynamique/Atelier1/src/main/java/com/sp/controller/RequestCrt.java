  package com.sp.controller;

  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.beans.factory.annotation.Value;
  import org.springframework.stereotype.Controller;
  import org.springframework.ui.Model;
  import org.springframework.web.bind.annotation.ModelAttribute;
  import org.springframework.web.bind.annotation.RequestMapping;
  import org.springframework.web.bind.annotation.RequestMethod;

import com.sp.model.Carte;
import com.sp.model.CarteFormDTO;
  
  @Controller // AND NOT @RestController
  public class RequestCrt {
  
  	@Value("${welcome.message}")
  	private String message;
  	
  	@Autowired
  	CarteDao carteDao;
  
  	private static String messageLocal="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
  
  	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
  	public String index(Model model) {
    
  		model.addAttribute("message", message);
  		model.addAttribute("messageLocal", messageLocal);
  
  		return "index";
  	}
  	
    @RequestMapping(value = { "/addCarte"}, method = RequestMethod.GET)
      public String addcarte(Model model) {
      	CarteFormDTO carteForm = new CarteFormDTO();
      	model.addAttribute("carteForm", carteForm);
      	return "carteForm";
      }

    @RequestMapping(value = { "/addCarte"}, method = RequestMethod.POST)
      public String addcarte(Model model, @ModelAttribute("carteForm") CarteFormDTO carteForm) {
    	Carte p = carteDao.addCarte(carteForm.getName(),carteForm.getDescription(),carteForm.getImgUrl(),carteForm.getFamily(),carteForm.getAffinity(), carteForm.getHp(),carteForm.getEnergy(), carteForm.getAttack(),carteForm.getDefence());
	    model.addAttribute("myCarte",p );
	    return "carteView";
    }
    
 	@SuppressWarnings("static-access")
	@RequestMapping(value = { "/view"}, method = RequestMethod.GET)
    public String view(Model model) {
    model.addAttribute("myCarte",carteDao.getRandomCarte() );
      return "carteView";
  }

    
    @RequestMapping(value = { "/list"}, method = RequestMethod.GET)
    public String viewList(Model model) {
  	  model.addAttribute("carteList",CarteDao.getCarteList() );
  	  return "carteViewList";
    }

   
 }