# Web Dynamique

__Attention__ : les tests se font sur le ports __8082__ .
Pour changer de port, aller dans src/main/resources/application.properties.

## Finalité
* le formulaire est créé dans le fichier carteForm.html. Pour y acceder, il faut faire __localhost:8082/addCarte__ dans un navigateur. Quand le formulaire est rempli, la carte s'affiche en cliquant sur +OK. Le bouton Cancel ne fonctionne pas. Pour le formulaire, il faut renseigner le nom, la description, une url d'une image, la family, l'affinity, le nombre d'hp, d'énergie, d'attaque et de défence.

* La Liste des cartes créé sont visibles : __localhost:8082/list__ . Seul le nom et la description s'affiche.