#  Architecture logicielle

## ATELIER I

### Questions


Réponses Questions

- Après avoir expliqué en quoi vos prototypes respectent le pattern MVC, expliquer les avantages et inconvénients des approches Web Statique + Web Service et Dynamique.

Les prototypes réalisés respectent le pattern MVC, car nous réalisons une séparation stricte des différents éléments utilisés pour servir la page web. En effet, nous avons d'un coté l'API qui sert de modèle, permettant de récupérer les données, d'un composant thymeleaf servant de vue, en générant et servant le code HTML, et enfin une partie spring boot qui sert de contrôleur, permettant de gérer les requêtes HTTP et de renvoyer les données au client.

- Expliquer les avantages et inconvénients des approches Web Statique + Web Service et Dynamique

Les avantages du serveur web statique sont sa grande flexibilité à s'adapter aux différentes plateformes, ainsi que sa capacité à réaliser des pages web différenciées et personnalisées. De plus, le temps de réponse est globalement le meme pour toutes les pages web, ce qui est un avantage lors de la création de sites web à forte fréquentation. En effet, les pages sont stockées sur le serveur à l'avance, il est donc possible de scaler facilement le serveur pour répondre à la demande.
Cependant, l'approche via web statique et web services présente quelques inconvénients. En effet, la capacité d'évolution et de maintien du site web est limitée en raison de la nécessité de recréer chaque page web lors de modifications, et de par l'absence de composants interactifs tel que des pages de connexion, ou des formulaires complexes.

 Enfin, les données sont stockées sur le serveur à l'avance, et récupérées via des requetes REST, ce qui facilite l'échange des données entre différentes plateformes.
statique => les +: flexibilité et capacité de réalistion et de différentiation des pages web, temps de réponse du serveur meme pour toutes pages web, données stockées sur le serveur à l'avance, récup via requetes REST, donc facilité d'échange des données entre différentes plateformes. Possibilité de scaler facilement
et les - : difficulté à maintenir et capacité d'évolution limitée, capacité d'adaption limitée, nécessite un plus grand délai de création des projets.

Les avantages du serveur web dynamique sont sa capacité à créer et modifier des pages web existantes rapidement, ainsi que de sa capacité à s'adapter aux différents utilisateurs en leur servant du contenu adapté par utilisateur. La pkupart des traitements sont également effectués coté serveur, ce qui permet de mieux répondre à des besoins pour des sites web accessibles depuis des terminaux mobiles ou des tablettes.
Cependant, les temps de réponse peuvent etre plus longs en fonction des requetes, et le stack technologique nécessaire pour maintenir et servir les pages web est plus complexe, notamment avec l'ajout de BDD et de services de frameworks pour gérer les différents aspects de sauvegarde et production de contenu.

dynamique => les + : rapidité de création des sites web, et de modification du contenu existant, ainsi que de capacité à adapter par utilisateur, réduction des traitements coté client, capacité à s'adapter aux différentes platformes, et à maintenir
et les - : Nécessite plus de connaissances pour développer, temps de réponse parfois lent en fonction des requetes, nécessite un stack technologique plus complexe pour servir et gérer les pages web, avec notamment des BDD et frameworks.


REPONSE:

- Après avoir expliqué en quoi vos prototypes respectent le pattern MVC, expliquer les avantages et inconvénients des approches Web Statique + Web Service et Dynamique.

Les prototypes réalisés respectent le pattern MVC, car nous réalisons une séparation stricte des différents éléments utilisés pour servir la page web. En effet, nous avons d'un coté l'API qui sert de modèle, permettant de récupérer les données, d'un composant thymeleaf servant de vue, en générant et servant le code HTML, et enfin une partie spring boot qui sert de contrôleur, permettant de gérer les requêtes HTTP et de renvoyer les données au client.

- Expliquer les avantages et inconvénients des approches Web Statique + Web Service et Dynamique

Les avantages du serveur web statique sont sa grande flexibilité à s'adapter aux différentes plateformes, ainsi que sa capacité à réaliser des pages web différenciées et personnalisées. De plus, le temps de réponse est globalement le meme pour toutes les pages web, ce qui est un avantage lors de la création de sites web à forte fréquentation. En effet, les pages sont stockées sur le serveur à l'avance, il est donc possible de scaler facilement le serveur pour répondre à la demande.
Cependant, l'approche via web statique et web services présente quelques inconvénients. En effet, la capacité d'évolution et de maintien du site web est limitée en raison de la nécessité de recréer chaque page web lors de modifications, et de par l'absence de composants interactifs tel que des pages de connexion, ou des formulaires complexes.

Les avantages du serveur web dynamique sont sa capacité à créer et modifier des pages web existantes rapidement, ainsi que de sa capacité à s'adapter aux différents utilisateurs en leur servant du contenu adapté par utilisateur. La pkupart des traitements sont également effectués coté serveur, ce qui permet de mieux répondre à des besoins pour des sites web accessibles depuis des terminaux mobiles ou des tablettes.
Cependant, les temps de réponse peuvent etre plus longs en fonction des requetes, et le stack technologique nécessaire pour maintenir et servir les pages web est plus complexe, notamment avec l'ajout de BDD et de services de frameworks pour gérer les différents aspects de sauvegarde et production de contenu.


#### Architecture logicielle et Web générale
- Qu’est-ce que le pattern MVC ? Quels avantages présente-t-il ? 

  Le pattern MVC est un pattern de conception qui permet de séparer les différentes parties d’une application en trois parties distinctes : le modèle, la vue et le contrôleur. Le modèle représente les données de l’application, la vue représente l’interface graphique de l’application et le contrôleur permet de gérer les interactions entre le modèle et la vue. Le pattern MVC permet de séparer les différentes parties d’une application et de rendre l’application plus facile à maintenir et à développer. Il permet également de rendre l’application plus modulaire et plus facilement réutilisable et maintenable dans d’autres applications.

- Qu’est-ce que le Web dynamique ? pourquoi est-il intéressant ? 

  Le web dynamique est un type de génération des pages web, qui sont générées et envoyées au client basées sur les informations récupérées et recoupées par le serveur depuis ses bases de données et ses fichiers. La page est remplie basée sur une template, qui est une page web qui contient des variables qui seront remplacées par des informations récupérées, permettant de les personnaliser selon les utilisateurs. 
  Ce type de site web est intéressant car il permet d'adapter à chaque utilisateur les informations qu'il reçoit, et donc de personnaliser l'expérience de l'utilisateur en fonction des besoins (par exemple, filtrer l'affichage en fonction du type d'utilisateur, de ses préférences, si il est connecté ou non, etc...), ainsi que de limiter les traitements réalisés côté client.

- Comment sont récupérés les fichiers par le Web Browser en Web statique ? Quels sont les avantages d’utiliser du Web Statique avec des services REST ?

  Sur un web statique, les fichiers sont récupérés à la demande du client via des requêtes HTTP GET, et sont déjà créés et stockés sur le serveur à l'avance. Avec des services REST, ce genre de site web est plus intéressant car il permet de réduire le temps de réponse du serveur; En effet, les requetes REST ont pour but de manipuler des ressources via leur représentation textuelle, et donc peuvent servir directement des pages web statiques déjà créées qui n'ont pas besoin d'être générées à chaque fois.


- Qu’est que les architectures N-Tiers ? Quelles avantages apportent-elles ?

  Les architectures N-Tiers sont un type d'architecture logicielle qui permet de séparer les différentes parties d'une application en couches distinctes. Chaque couche est chargée d'une partie spécifique de l'application, et les couches sont indépendantes les unes des autres. Les trois couches les plus courantes sont la couche présentation, responsable de l'affichage et de la communication avec l'utilisateur, la couche métier, responsable de la mise en oeuvre des règles de gestion et de la logique applicative, et la couche de données, responsable de la gestion et stockage des données long terme.
  L'architecture N-tiers permet de découpler les différentes parties de l'application, ce qui apporte une évolutivité des différentes couches de manière indépendante, une plus grande facilité à échanger des données entre différentes plateformes, ainsi qu'une facilité à maintenir et développer l'application.

- Comment fonctionne l’AJAX ?

  AJAX est un concept de progrmmation web permettant de récupérer des données depuis un serveur sans avoir à recharger les pages entières.
  Pour ce faire, les pages web avec AJAX utilisent des scripts Javascript en fond de page et exécutés par le navigateur, qui, lors de manipulations de l'utilisateur, envoient des requetes HTTP en arrière-plan au serveur, dont les résultats vont ensuite permettre de mettre à jour le contenu de la page web sans avoir à la recharger.

#### JEE
 - Qu’est-ce que JEE ? 

  JEE est un standard de développement d'applications web permettant de spécifier la manière dont les applications web doivent être développées, en spécifiant le comportement pour la génération des pages, ainsi que les services web et les bases de données.

- Comment fonctionne un serveur JEE ? Qu’est-ce qu’un Web Container en JEE ?

  Un serveur JEE fonctionne en utilisant des API (servlets) permettant de spécifier ce qui doit etre fourni, en découplant l'implémentation du code, et forcant la réécriture des API. Un web container est un composant de JEE permettant de gérer l'exécution des pages web, servlets (API), et composants pour le serveur JEE.

- Qu’est ce que Springboot ? quelles différences avec JEE ?

  Springboot est un framework JEE qui permet de créer des applications web en Java, tout en permettant de simplifier la création à travers d'outils pour la génération de page web et de bases de données. 
  La différence entre Spring et JEE est que JEE est un standard, tandis que Spring est un framewrok basé sur ce dernier. Spring apporte donc des outils pour simplifier la création d'applications web, et permet de créer des applications web plus rapidement.

#### Springboot
- Qu’apport Thymeleaf à Springboot ?

  Theameleaf est un moteur de template, il apporte des outils pour créer et gérer des modèles de pages web pour le serveur, qui va ensuite générer les pages web à partir de ces modèles. Ces modèles apportent une facilité et rapidité de création couplé à une facilité à les modifier et maintenir.

- Que signifie l’annotation @Controller, pourquoi est-elle importante lors de la génération de pas coté serveur ?

  L'annotation @Controller permet de spécifier que la classe est un contrôleur, et va donc permettre de gérer la création des modèles de page web à générer et de les rendre au client. Sans cette annotation, la classe n'est pas reconnue par springboot et ne sera pas utilisée pour la génération.

- Que représente le répertoire ‘src/main/resources’ dans un projet SpringBoot ? Quelles sont les informations stockées à cet endroit ?

  Le répertoire 'src/main/resources' représente le dossier 'resources' du projet, permettant de stocker des fichiers qui auront besoin d'etre accessibles pour le projet. Ce dossier stocke notamment les fichiers de configuration, les fichiers de données et les fichiers de templates pour la génération des pages web.

- Les avantages et les inconvénients du Web Statique et Web Dynamique


  Les avantages du web statique sont qu'il permet de réduire le temps de réponse du serveur, et qu'il permet de réduire la consommation de ressources du serveur, et ses inconvénients sont sa difficulté à maintenir et à développer, ainsi que la difficulté à échanger des données entre différentes plateformes.
  Les avantages du web dynamique sont qu'il permet de créer des sites web plus rapidement,, avec une grande variété de page, et ses inconvénients sont qu'il est plus difficile à maintenir et à développer, et qu'il est plus difficile à échanger des données entre différentes plateformes + temps de réponse long.s